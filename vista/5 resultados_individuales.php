<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Encuesta de Seguridad</title>
  
  
  <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>

  <link rel="stylesheet" href="../assets/css/test15.css">
  

</head>

<body onload="inicio()">





  <section class="menu menu--circle">
  <input type="checkbox" id="menu__active"/>
  <label for="menu__active" class="menu__active">
    <div class="menu__toggle">
      <div class="icon">
        <div class="hamburger"></div>
      </div>
    </div>
    <input type="radio" name="arrow--up" id="degree--up-0"/>
    <input type="radio" name="arrow--up" id="degree--up-1" />
    <input type="radio" name="arrow--up" id="degree--up-2" />
    <div class="menu__listings">
      <ul class="circle">
        <li>
          <div class="placeholder">
            <div class="upside">
              <a href="../controlador/5 resultados_individuales.php?usuario=<?php echo($id); ?>" class="button" title="Resultados individuales"><i class="fa fa-bar-chart"></i></a>
            </div>
          </div>
        </li>
        <li>
          <div class="placeholder">
            <div class="upside">
              <a href="../controlador/3 encuesta.php?usuario=<?php echo($id); ?>" class="button" title="Realizar encuesta"><i class="fa fa-align-justify"></i></a>
            </div>
          </div>
        </li>
        <li>
          <div class="placeholder">
            <div class="upside">
              <a href="#">&nbsp</a>
            </div>
          </div>
        </li>
        <li>
          <div class="placeholder">
            <div class="upside">
              <a href="#" class="button"><i class=""></i></a>
            </div>
          </div>
        </li>
        <li>
          <div class="placeholder">
            <div class="upside">
              <a href="#" class="button"><i class=""></i></a>
            </div>
          </div>
        </li>
        <li>
          <div class="placeholder">
            <div class="upside">
              <a href="#" class="button"><i class=""></i></a>
            </div>
          </div>
        </li>
        <li>
          <div class="placeholder">
            <div class="upside">
              <a href="../index.php" class="button" title="Cerrar Sesión"><i class="fa fa-sign-out"></i></a>
            </div>
          </div>
        </li>
        <li>
          <div class="placeholder">
            <div class="upside">
              <a href="6 resultados_empresa.php?usuario=<?php echo($id); ?>" class="button" title="Resultados por empresa"><i class="fa fa-building"></i></a>
            </div>
          </div>
        </li>
        <li>
          <div class="placeholder">
            <div class="upside">
              <a href="6 resultados_empresa.php?usuario=<?php echo($id); ?>" class="button" title="Resultados por empresa" class="button" title="Graficas de resultado empresa"><i class="fa fa-pie-chart"></i></a>
            </div>
          </div>
        </li>
        <li>
          <div class="placeholder">
            <div class="upside">
              <a href="../controlador/5.2 listar_resultado_dominio.php?usuario=<?php echo($id); ?>" class="button" title="Resultados por dominios"><i class="fa fa-line-chart"></i></a>
            </div>
          </div>
        </li>
      </ul>
    </div>
    <div class="menu__arrow menu__arrow--top">
      <ul>
        <li>
          <label for="degree--up-0"><div class="arrow"></div></label>
          <label for="degree--up-1"><div class="arrow"></div></label>
          <label for="degree--up-2"><div class="arrow"></div></label>
        </li>
      </ul>
    </div>
  </label>
</section>
  
   
    <div id="margen" class="<?php echo($id); ?>"></div>
    
    <div id="titulo">
        <h1>Test de seguridad</h1>
    </div>
    
    <div id="cuerpo">
        <h2>Resultados y recomendaciones por preguntas:</h2>
    </div>
    
    <div id="cuerpo_preguntas" style="width: 1000px">
       
       
        
    </div>

    <a id='logout' href='../index.php' title="Cerrar Sesión"><i class="fa fa-sign-out"></i></a>


    <script src="../assets/js/jquery-3.2.1.js"></script>
    <script src="../assets/js/conclusiones5.js"></script>


</body>

</html>
