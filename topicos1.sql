-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 04-12-2019 a las 02:15:27
-- Versión del servidor: 5.7.21
-- Versión de PHP: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `topicos1`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conclusionp`
--

DROP TABLE IF EXISTS `conclusionp`;
CREATE TABLE IF NOT EXISTS `conclusionp` (
  `numero` int(3) NOT NULL AUTO_INCREMENT,
  `sector` varchar(90) COLLATE utf8_spanish_ci NOT NULL,
  `conclusion` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `tempo` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`numero`)
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `conclusionp`
--

INSERT INTO `conclusionp` (`numero`, `sector`, `conclusion`, `tempo`, `fecha`) VALUES
(1, 'Administración_Activos', 'Se recomienda que el gerente administrativo debe revisar los activos no identificados y mantener el inventario o registre con todos los activos importantes.', 'Cada Año', 'Desde 20/08/2019 al 01/02/2020'),
(2, 'Administración_Activos', 'Se recomienda que el  gerente administrativo debe identificar los activos si tiene un dueño, una clasificación de seguridad definida y acordada y restricción de accesos los cuales deben ser revisados regularmente.', 'Cada Semestre', 'Desde 15/08/19 al 17/12/2019'),
(3, 'Administración_Activos', 'Se recomienda que el secretario general deba revisar y poner la información en clasificado en términos de valor, requisitos legales, sensibilidad y criticidad para la organización.', 'Cada Año', 'Desde 18/10/19 al 20/02/2020'),
(4, 'Seguridad_Recursos_Humanos', 'Se recomienda que recursos humanos debe revisar si fueron definidos y documentados los roles y responsabilidades de seguridad de los empleados, contratistas y terceros, que tengan de acuerdo con la política de seguridad informática de la organización.', 'Cada 2 Años', 'Desde 02/11/19 al 21/02/20'),
(5, 'Seguridad_Recursos_Humanos', 'Se recomienda que recursos humanos efectue una revisión de antecedentes para todos los candidatos a emplear, contratistas y terceros según regulaciones relevantes.', 'Cada Año', 'Desde 15/12/19 al 15/01/20'),
(6, 'Seguridad_Recursos_Humanos', 'Recursos humanos debe crear un proceso disciplinario formal para los empleados que han cometido una falta en seguridad.', 'Cada Año', 'Desde 02/11/19 al 04/11/19'),
(7, 'Seguridad_Recursos_Humanos', 'Se recomienda que recursos humanos se encargue de revisar y verificar que el derechos de acceso de todos los empleados, contratistas y terceros a informaciones o facilidades informáticas, los cuales serán eliminados al termino de su relación laboral, contrato o acuerdo o será ajustado en caso de cambios.', 'Cada Año', 'Desde 4/12/19 al 6/12/19'),
(8, 'Seguridad_Física_y_Ambiental', 'Se recomienda el uso de una tarjeta de autentificacion.', 'Cada Año', 'Desde 21/11/19 al 15/06/20'),
(9, 'Seguridad_Física_y_Ambiental', 'Se recomienda el uso de extintores en caso de incensdios y vias de acceso rapido para otros desastres naturales.', 'Cada Trimestre', 'Desde 20/12/19 al 20/02/20'),
(10, 'Seguridad_Física_y_Ambiental', 'Se recomienda realizar contratos y seguros con los dueños del predio por cualquier inconveniente en el futuro.', 'Cada Semestre', 'Desde 16/12/19 al 21/02/20'),
(11, 'Seguridad_Física_y_Ambiental', 'Se recomienda el uso de respaldos en caso de cualquier amenaza ambiental.', 'Cada Año', 'Desde 18/02/20 al 18/04/20'),
(12, 'Seguridad_Física_y_Ambiental', 'Se recomienda el uso de reguladores de voltaje', 'Diario', 'Desde 23/11/19 al 17/03/20'),
(13, 'Seguridad_Física_y_Ambiental', 'Se recomienda el uso de Ups(proteccion de caidas y bajadas electricas)', 'Cada 6 Meses', 'Desde 21/07/19 al 21/01/20'),
(14, 'Seguridad_Física_y_Ambiental', 'Se recomienda el uso de los 6 mantenimientos de software que son preventivo, predictivo, correctivo, adaptativo, evolutivo, perfectivo.', 'Cada Año', 'Desde 22/08/19 al 10/02/20'),
(15, 'Seguridad_Física_y_Ambiental', 'Se recomienda tener una tarjeta de autorizacion por cada personal de mantenimiento', 'Cada Año', 'Desde 15/01/20 al 15/02/20'),
(16, 'Seguridad_Física_y_Ambiental', 'Se recomienda monitorear los equipos.', 'Diario', 'Desde 30/01/20 al 19/03/20'),
(17, 'Seguridad_Física_y_Ambiental', 'Se recomienda contratar seguros para la compañia.', 'Cada Año', 'Desde 21/01/20 al 22/02/20'),
(18, 'Seguridad_Física_y_Ambiental', 'Se recomienda tener guardias de seguridad para el hardware y tener encriptacion en el software.', 'Diario', 'Desde 24/09/19 al 15/10/19'),
(19, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener documentos de memoria para poder realizar informes de toda la documentacion.', 'Cada Año', 'Desde 05/08/19 al 15/12/19'),
(20, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener distintos servidores para poder realizar las pruebas de una manera aislada.', 'Cada 15 Dias', 'Desde 21/02/20 al 17/04/20'),
(21, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener programas de monitoreos ti como panadora FMS.', 'Diario', 'Desde 18/02/20 al 21/04/20'),
(22, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener programas de monitoreos de auditorias como auditool.', 'Diario', 'Desde 22/01/20 al 07/03/20'),
(23, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda verificar todas las semanas actualizaciones de los programas que la empresa esta usando.', 'Semanal', 'Desde 18/01/20 al 12/03/20'),
(24, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener antivirus en la empresa como avast.', 'Cada Año', 'Desde 31/02/20 al 15/03/20'),
(25, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener antivirus en la empresa como avast.', 'Cada Año', 'Desde 31/02/20 al 15/03/20'),
(26, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener todos los sistemas configurados de la misma forma para que el uso de codigo movil sea instantaneo.', 'Cada 2 meses', 'Desde 30/10/19 al 18/11/19'),
(27, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener todos los sistemas encriptados y configurados de una forma que sea dificil realizar el codigo movil por una persona no autorizada.', 'Cada 2 meses', ' Desde 30/10/19 al 18/11/19'),
(28, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener distintos respaldos para las informaciones.', 'Cada semana', 'Desde 21/02/20 al 17/04/20'),
(29, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener un documento de memoria para poder realizar informes intuitivos sobre la informacion.', 'Cada Año', 'Desde 05/08/19 al 15/12/19'),
(30, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener programas para desechar la informarcion de forma segura como iobit', 'Cada 2 meses', 'Desde 30/10/19 al 18/11/19'),
(31, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener una base de datos particular para poder almacenar la informacion.', 'Cada Año', 'Desde 17/03/20 al 17/04/20'),
(32, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener encriptada la informacion para que no ingresen personas no autorizadas.', 'Cada Año', 'Desde 12/03/20 al 12/04/20'),
(33, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener acuerdos con otras empresas como mailify, que es una emprema especializada en la trasferencia de datos.', 'Cada 5 Años', 'Desde 13/08/19 al 30/09/19'),
(34, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener monitorizacion cuando se traslada la informacion.', 'Diaria', 'Desde 29/02/20 al 29/03/20'),
(35, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda al realizar una transaccion que la pagina tenga en un principio la sigla de https.', 'Cada 6 meses', 'Desde 23/03/20 al 23/04/20'),
(36, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda al realizar una transaccion que la pagina tenga en un principio la sigla de https.', 'Cada 6 meses', 'Desde 23/03/20 al 23/04/20'),
(37, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener un metodo de encriptacion para la informacion publica como una criptografía híbrida.', 'Cada Año', 'Desde 15/03/20 al 15/06/20'),
(38, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener programas de monitoreos como zabbix.', 'Diaria', 'Desde 29/02/20 al 29/03/20'),
(39, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener programas de monitoreos como zabbix.', 'Diaria', 'Desde 29/02/20 al 29/03/20'),
(40, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener una base de datos privada y encripatada para que se mas dificil el ingreso de personas no autorizada.', 'Cada Año', 'Desde 05/08/19 al 15/12/12'),
(41, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener una base de datos privada y encripatada para que se mas dificil el ingreso de personas no autorizada.', 'Cada Año', 'Desde 05/08/19 al 15/12/12'),
(42, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener a un empleado que monitore constantemente estos registros.', 'Diario', 'Desde 29/02/20 al 29/03/20'),
(43, 'Administración_de_Incidentes_de_Seguridad_Informática', 'Se recomienda que el encargado de seguridad informática implemente un canal de administración, y un protocolo para cumplir dicho canal', 'Diario', 'Desde 27/04/20 al 10/05/20'),
(44, 'Administración_de_Incidentes_de_Seguridad_Informática', 'Se recomienda que el encargado de seguridad informática implemente un procedimiento idóneo para notificación y reporte de debilidades , y sospechas en el sistema de seguridad.', 'Diario', 'Desde 31/02/20 al 15/03/20'),
(45, 'Administración_de_Incidentes_de_Seguridad_Informática', 'Se recomienda que el encargado de administración asegurar que los procedimientos y responsabilidades administrativas se establescan de forma rápida, efectiva y ordenada, ante cualquier incidente de seguridad informática.', 'Cada 6 Meses', 'Desde 08/06/20 al 30/06/20'),
(46, 'Administración_de_Incidentes_de_Seguridad_Informática', 'Se recomienda al encargado de seguridad informática implemetar un mecanismo para identificar y cuantificar el tipo, volumen y costos de los incidentes de seguridad informáticos.', 'Diario', 'Desde 04/07/20 al 25/07/20'),
(47, 'Administración_de_Incidentes_de_Seguridad_Informática', 'Se recomienda que el encargado de seguridad informática realice actividades de seguimiento, incluoso con consecuencias legales, si el incidente lo amerita(civiles o criminales).', 'Diario', 'Desde 26/07/20 al 12/08/20'),
(48, 'Administración_Continuidad_de_Negocios', 'Se recomienda al encargado de seguridad informática implementar un proceso administrado con los requisitos de seguridad informática para el desarrollo y mantenimiento de la continuidad de negocios a través de la organización.', 'Cada Año', 'Desde 16/10/20 al 30/10/20'),
(49, 'Administración_Continuidad_de_Negocios', 'Se recomienda al encargado de seguridad informática implementar procesos que identifiquen eventos, así como su probabilidad de impacto, y consecuencias, que interrumpan procesos de negocio.', 'Diario', 'Desde 29/11/20 al 17/12/20'),
(50, 'Administración_Continuidad_de_Negocios', 'Se recomienda al encargado de seguridad informática revisar los planes de mantención y restauración de las operaciones de negocio, deberán asegurar la disponibilidad de la información dentro de los niveles requeridos seuidos a una interrupción o falla a los procesos de negocios.', 'Cada Año', 'Desde 18/12/20 al 10/01/21'),
(51, 'Administración_Continuidad_de_Negocios', 'Se recomienda que el gerente administrativo implemente más de una arquitectura para el Plan de Continuidad de Negocios.administrado con los requisitos de seguridad informática para el desarrollo y mantenimiento de la continuidad de negocios a través de la organización.', 'Cada Año', 'Desde 28/01/20 al 16/02/20'),
(52, 'Administración_Continuidad_de_Negocios', 'Se recomienda que el gerente administrativo, en conjunto con el jefe de seguridad  generen un procedimiento regular de pruebas  parq que se garantice que los Planes de Continuidad de Negocios estén actualizados y sean efectivos.', 'Cada 6 Meses', 'Desde 28/03/20 al 05/04/20'),
(53, 'Cumplimiento', 'Se recomienda al encargado administrativo se asegure que todos los requisitos relevantes a regulaciones, contratos, etc. y el enfoque organizativo cumplan los requisitos, es estos queden definidos explícitamente y documentados para cada sistema. informático y organización.', 'Cada 3 Meses', 'Desde 21/05/20 al 13/06/20'),
(54, 'Cumplimiento', 'Se recomienda que el encargado administrativo implemente procedimientos que garanticen el cumplimiento con requisitos legislativos, regulaciones y contratos en el uso de material en el cual pueden existir derechos de propiedad intelectual y en el uso de productos de software propietarios.', 'Cada 3 Meses', 'Desde 14/06/20 al 30/06/20'),
(55, 'Cumplimiento', 'Se recomienda que el encargado administrativo se asegure que los registros importantes de la organización son protegidos de perdidas, destrucción y falsificación de acuerdo con regulaciones, contratos y requisitos de negocios.', 'Cada Año', 'Desde 02/07/20 al 15/07/20'),
(56, 'Cumplimiento', 'Se recomienda que el encargado administrativo se asegure que la protección y privacidad de los datos es asegurada según legislación, regulación relevantes y si es aplicable según cláusulas contractuales.', 'Cada Año', 'Desde 16/07/20 al 05/08/20'),
(57, 'Cumplimiento', 'Se recomienda que el gerente  defina el uso de las facilidades de procesos informáticos para efectos personales o no autorizados sin el consentimiento de la gerencia sea tratado como uso inapropiado.', 'Cada Año', 'Desde 06/08/20 al 27/08/20'),
(58, 'Cumplimiento', 'Se recomienda al encargado de seguridad informática utilizar controles criptográficos  en cumplimiento con acuerdo, leyes y regulaciones relevantes.', 'Cada Año', 'Desde 28/08/20 al 12/09/20'),
(59, 'Cumplimiento', 'Se recominenda a los gerentes que aseguren que todos los procedimientos de seguridad dentro de sus áreas de responsabilidad sean efectuados correctamente para lograr cumplimiento con las políticas y estándares de seguridad.', 'Cada Año', 'Desde 18/02/20 al 10/03/20'),
(60, 'Cumplimiento', 'Se recomienda ala gerente de seguridad que  los sistemas informáticos sean regularmente revisados, en relación al cumplimiento con la implementación de estándares de seguridad.', 'Cada 2 Meses', 'Desde 11/03/20 al 27/03/20'),
(61, 'Cumplimiento', 'Se recomienda al encargado de seguridad planear cuidadosamente las revisiones de los sistemas operacionales y acordadas para minimizar los riesgos o interrupciones a los procesos de negocio.', 'Cada Año', 'Desde 26/04/20 al 10/05/20'),
(62, 'Cumplimiento', 'Se recomienda al encargado de seguridad prevenir posible mal uso o compromisos de herramientas de auditoria de sistemas como software o archivos de datos son protegidos.', 'Cada Año', 'Desde 11/05/20 al 05/06/20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conclusion_intermedia`
--

DROP TABLE IF EXISTS `conclusion_intermedia`;
CREATE TABLE IF NOT EXISTS `conclusion_intermedia` (
  `numero` int(11) NOT NULL AUTO_INCREMENT,
  `sector` varchar(90) COLLATE utf8_spanish_ci NOT NULL,
  `conclusion2` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `tempo` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`numero`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `conclusion_intermedia`
--

INSERT INTO `conclusion_intermedia` (`numero`, `sector`, `conclusion2`, `tempo`, `fecha`) VALUES
(1, 'Administración_Activos', 'Se recomienda que el gerente administrativo revise los activos no identificados y mantener los inventarios o registros con los activos importantes, ademas se deben revisar e indentificar si tiene algun dueño, una clasificacion de seguridad definida y acordada y registro de accesos, los cuales deben ser revisados regularmente.', 'Cada año - Cada semestre', 'Desde 15/08/19 al 20/02/20'),
(2, 'Seguridad_Recursos_Humanos', 'Se recomienda que se efectue la revision de antecedentes para todos los candidatos a emplear, contratistas y terceros segun regulaciones relevantes , Tambien que creen un proceso diciplinario formal para los empleados que han cometid una falta en seguridad. La ejecucion de la recomendacion se iniciará el 25 de junio y finaliazrá el 25 de julio', 'Cada 2 y 1 año', 'Desde 15/12/19 al 21/02/20'),
(3, 'Seguridad_Física_y_Ambiental', 'Se recomienda que el gerente administrativo se preocupe de tener un un metodo  de autentificacion de todos los roles, se recomienda un encargado para el  monitoreo total de todas las funciones y de realizar acuerdos de \"palabras\"con externos para que no exista ningun problema a futuro.', 'Cada Año - Diario', 'Desde 21/07/19 al 15/06/20'),
(4, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener  un empleado encargado de preocuparse de que todos los servidores esten encriptados y configurados de la misma forma para que sea mas dificil realizar una codificacion movil para personas externas, se recomiendo tener a un empleado la creacion, configuracion y revision de la base de datos privadas para que sea mas dificil para usuarios externos poder ingresar a ella.', 'Cada 5 años - diario', 'Desde 05/08/19 al 15/06/20'),
(5, 'Administración_de_Incidentes_de_Seguridad_Informática', 'Se recomienda que los encargados de administración y seguridad informática implementen, de mejor manera, canales y protocolos para la revisión minusiosa de los procesos relacionados con oincidentes de seguridad informática. La ejecucion de la recomendacion se iniciará el 25 de junio y finalizará el 5 de julio.', 'Cada 6 Meses- Diario', 'Desde 31/02/20 al 12/08/20'),
(6, 'Administración_Continuidad_de_Negocios', 'Se recomienda una mayor fluidez entre los procesos conjuntos entre el Plan de continuidad de negocios y el plan de seguridad en función de los obejtivos codependientes, pare ello se deberá realizar el desarrollo óptimo de todos los puntos deficientes. La ejecucion de la recomendacion se iniciará el 25 de junio y finalizará el 15 de julio.', 'Cada Año - Diario', 'Desde 28/01/20 al 10/01/21'),
(7, 'Cumplimiento', 'Se recomienda que se desarrollen los diferentes elementos de seguridad, a modo que se cumplan a cabalidad las regulaciones contratos,', 'Cada Año - Cada 2 Meses', 'Desde 18/02/20 al 12/09/20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `conclusion_mala`
--

DROP TABLE IF EXISTS `conclusion_mala`;
CREATE TABLE IF NOT EXISTS `conclusion_mala` (
  `numero` int(11) NOT NULL AUTO_INCREMENT,
  `sector` varchar(90) COLLATE utf8_spanish_ci NOT NULL,
  `conclusion` varchar(1000) COLLATE utf8_spanish_ci NOT NULL,
  `tempo` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`numero`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `conclusion_mala`
--

INSERT INTO `conclusion_mala` (`numero`, `sector`, `conclusion`, `tempo`, `fecha`) VALUES
(1, 'Administración_Activos', 'Se recomienda que el gerente administrativo revise los activos no identificados y mantener los inventarios o registros con los activos importantes, ademas se deben revisar e indentificar si tiene algun dueño, una clasificacion de seguridad definida y acordada y registro de accesos, los cuales deben ser revisados regularmente. Por parte del secretario general deberá revisar y poner la informacion en clasificado en terminos de valor, requisitos legales, sensibilidad y criticidad para la organizacion.', 'Cada año - Cada Semestre', 'Desde 15/08/19 al 20/02/20'),
(2, 'Seguridad_Recursos_Humanos', 'Se recomienda en primer lugar que Recursos Humanos revise si fueron definidos y documentados los roles y responsabilidades de seguradad de los empleados y estos tengan acuerdo con la politica de seguridad de la organizacion. En segundo lugar, que efectuen la revision de antecedentes de los futuros candidatos a emplear, contratistas y terceros segun regulaciones relevantes. En tercer lugar que creen un proceso diciplinario formal para los empleados que cometan falta en la seguridad. Y en cuarto lugar que se encarguen de revisar y verificar que el derecho de los accesos de los empleados, contratista y terceros a informaciones o facilidades informaticas , los cuales serán eliminado al ermino de su relacion laboral.  La ejecucion de la recomendacion se iniciará el 25 de junio y finaliazrá el 25 de julio', 'Cada 2 y 1 año', 'Desde 15/12/19 al 21/02/20'),
(3, 'Seguridad_Física_y_Ambiental', 'Se recomienda que el gerente administrativo se preocupe de tener mas de un metodo  de autentificacion de todos los roles, se recomienda mas de un encargado para el  monitoreo total de todas las funciones y de realizar contratos con externos para que no exista ningun problema a futuro.', 'Cada Año - Diario', 'Desde 21/07/19 al 15/06/20'),
(4, 'Administración_Comunicaciones_y_Operaciones', 'Se recomienda tener a mas de un empleado encargado de preocuparse de que todos los servidores esten encriptados y configurados de la misma forma para que sea mas dificil realizar una codificacion movil para personas externas, se recomiendo tener a mas de un empleado la creacion, configuracion y revision de la base de datos privadas para que sea mas dificil para usuarios externos poder ingresar a ella.', 'Cada 5 Años - Diario', 'Desde 05/08/19 al 15/06/20'),
(5, 'Administración_de_Incidentes_de_Seguridad_Informática', 'Se recomienda que los encargados de administración y seguridad informática realicen implementaciones de canales, como así de protocolos para la revisión minusiosa de los procesos relacionados conoincidentes de seguridad informática. La ejecucion de la recomendacion se iniciará el 25 de junio y finalizará el 15 de julio.', 'Cada 6 Meses - Diario', 'Desde 31/02/20 al 12/08/20'),
(6, 'Administración_Continuidad_de_Negocios', 'Se recomienda que exista planificación de los procesos conjuntos entre el Plan de continuidad de negocios y el plan de seguridad en función de los obejtivos codependientes. ES por esto que se deberá realizar el desarrollo óptimo y completo de todos los puntos deficientes y faltantes. La ejecucion de la recomendacion se iniciará el 25 de junio y finalizará el 25 de julio.', 'Cada Año - Diario', 'Desde 05/08/19 al 15/06/20'),
(7, 'Cumplimiento', 'Se recomienda que se implemeten los diferentes elementos de seguridad, a modo que se cumplan a cabalidad las regulaciones contratos, a nivel de protocolos y de legalidad en los que estos se relacionen, así como también las políticas dey estándares de seguridad, así como las auditorías de sistemas. La ejecucion de la recomendacion se iniciará el 25 de junio y finalizará el 25 de julio.', 'Cada Año - Cada 2 Meses', 'Desde 18/02/20 al 12/09/20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

DROP TABLE IF EXISTS `empresa`;
CREATE TABLE IF NOT EXISTS `empresa` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `rut` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`id`, `nombre`, `rut`) VALUES
(1, 'Forestal', '18540177');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas`
--

DROP TABLE IF EXISTS `preguntas`;
CREATE TABLE IF NOT EXISTS `preguntas` (
  `numero` int(4) NOT NULL AUTO_INCREMENT,
  `sector` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `sub` varchar(120) COLLATE utf8_spanish_ci NOT NULL,
  `preguntas` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`numero`)
) ENGINE=MyISAM AUTO_INCREMENT=63 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `preguntas`
--

INSERT INTO `preguntas` (`numero`, `sector`, `sub`, `preguntas`) VALUES
(1, 'Administración_Activos', 'Responsabilidad sobre Activos', 'Si todos los activos han sido identificados y un inventario o registro se mantiene con todos los activos importantes.'),
(2, 'Administración_Activos', 'Responsabilidad sobre Activos', 'SI cada activo identificado tiene un dueño, una clasificación de seguridad definida y acordada y restricción de accesos que son revisados regularmente.'),
(3, 'Administración_Activos', 'Responsabilidad sobre Activos', 'Si la información es clasificada en términos de valor, requisitos legales, sensibilidad y criticidad para la organización.'),
(4, 'Seguridad_Recursos_Humanos', 'Previo a contratación', 'Si la administración requiere que los empleados, contratistas y terceros apliquen seguridad de acuerdo con políticas y procedimientos establecidos por la organización.'),
(5, 'Seguridad_Recursos_Humanos', 'Previo a contratación', 'Si todos los empleados en la organización, y donde sea relevante, contratistas y terceros reciben entrenamiento adecuado de seguridad como también actualizaciones regulares en las políticas y procedimientos organizacionales correspondiente a su funciones.'),
(6, 'Seguridad_Recursos_Humanos', 'Durante el empleo', 'Si existe un proceso disciplinario formal para los empleados que han cometido una falta en seguridad.\r\n'),
(7, 'Seguridad_Recursos_Humanos', 'Termino o transferencia laboral', 'Si los derechos de acceso de todos los empleados, contratistas y terceros a informaciones o facilidades informáticas, serán eliminados al termino de su relación laboral, contrato o acuerdo o será ajustado en caso de cambios.\r\n'),
(8, 'Seguridad_Física_y_Ambiental', 'Área Segura', 'Que controles de acceso existen para solo permitir el ingreso de personal autorizado dentro de varias áreas de la organización.\r\n'),
(9, 'Seguridad_Física_y_Ambiental', 'Área Segura', 'Si los cuartos que contiene los servicios de procesamientos informáticos están cerrados o contiene gabinetes o cajas de seguridad cerradas.\r\n'),
(10, 'Seguridad_Física_y_Ambiental', 'Área Segura', 'Si la protección física contra daños de fuego, inundaciones, terremotos, explosiones, disturbios civiles y otras formas de desastres ya sean naturales o causados por el hombre deben ser diseñados y aplicados.\r\n'),
(11, 'Seguridad_Física_y_Ambiental', 'Seguridad Equipos', 'Si los equipos han sido protegidos para reducir los riesgos de amenazas y peligros ambientales como oportunidades de accesos no autorizados.\r\n'),
(12, 'Seguridad_Física_y_Ambiental', 'Seguridad Equipos', 'Si los equipos están protegidos de fallas eléctricas como también de otras fallas que pueden ser causadas por fallas en utilitarios de apoyo.\r\n'),
(13, 'Seguridad_Física_y_Ambiental', 'Seguridad Equipos', 'Si los cables de energía eléctrica y telecomunicaciones que transmiten datos o apoyan los servicios informáticos son protegidos de intercepciones o daños.\r\n'),
(14, 'Seguridad_Física_y_Ambiental', 'Seguridad Equipos', 'Si los equipos son mantenidos correctamente para asegurar su disponibilidad e integridad.\r\n'),
(15, 'Seguridad_Física_y_Ambiental', 'Seguridad Equipos', 'Si el mantenimiento solo es efectuado por personal autorizado\r\n'),
(16, 'Seguridad_Física_y_Ambiental', 'Seguridad Equipos', 'Si controles apropiados han sido implementados cuando se envían equipos fuera de las oficinas.\r\n'),
(17, 'Seguridad_Física_y_Ambiental', 'Seguridad Equipos', 'Si el equipo esta cubierto por seguros y si los requisitos del seguro se cumplen.\r\n'),
(18, 'Seguridad_Física_y_Ambiental', 'Seguridad Equipos', 'Si existen controles para que los equipos, información y software no sean llevados fuera de la organización sin previa autorización.\r\n'),
(19, 'Administración_Comunicaciones_y_Operaciones', 'Procedimiento Operacional y Responsabilidades', 'Si el procedimiento operacional esta documentado, mantenido y disponible para todos los usuarios que lo necesiten.\r\n'),
(20, 'Administración_Comunicaciones_y_Operaciones', 'Procedimiento Operacional y Responsabilidades', 'Si las facilidades de desarrollo y pruebas están aisladas de las facilidades operacionales. Ejemplo software de desarrollo solo debe correr en un computador diferente a aquel en que se corre software de producción. Donde sea necesario las redes de desarrollo y producción deben mantenerse separadas.\r\n'),
(21, 'Administración_Comunicaciones_y_Operaciones', 'Administración entrega de Servicios a Terceros', 'Si los servicios, reportes y registros proveídos por terceros son regularmente monitoreados y revisados.\r\n'),
(22, 'Administración_Comunicaciones_y_Operaciones', 'Administración entrega de Servicios a Terceros', 'Si se efectúan auditorias sobre los servicios de terceros, reportes y registros en forma reiterada.\r\n'),
(23, 'Administración_Comunicaciones_y_Operaciones', 'Planeacion y Aceptación de Sistemas', 'Si criterios para la aceptación de sistemas han sido establecidos para nuevos sistemas informáticos, actualizaciones y nuevas versiones.\r\n'),
(24, 'Administración_Comunicaciones_y_Operaciones', 'Protección contra código malicioso y móvil', 'Si controles para la detección, prevención y control de recuperación para proteger contra código malicioso como también procedimientos de concientizacion del usuario han sido desarrollados e implementados.\r\n'),
(25, 'Administración_Comunicaciones_y_Operaciones', 'Protección contra código malicioso y móvil', 'Si solo se usa código móvil autorizado. (Código móvil es aquel software que se transfiere de un computador a otro y luego se ejecuta automáticamente. Efectúa un función especifica con con poca o ninguna intervención del usuario. Código móvil esta asociado con un numero de servicios middleware.)\r\n'),
(26, 'Administración_Comunicaciones_y_Operaciones', 'Protección contra código malicioso y móvil', 'Si la configuración asegura que el código móvil autorizado opera según las políticas de seguridad.\r\n'),
(27, 'Administración_Comunicaciones_y_Operaciones', 'Protección contra código malicioso y móvil', 'Si la ejecución de código móvil no autorizado es evitado.\r\n'),
(28, 'Administración_Comunicaciones_y_Operaciones', 'Respaldo / Backup', 'Si toda información y software esencial puede ser recuperada seguido de un desastre o falla de medio.\r\n'),
(29, 'Administración_Comunicaciones_y_Operaciones', 'Manejo de Medios', 'Si todos los procedimientos y niveles de autorización son claramente definidos y documentados.\r\n'),
(30, 'Administración_Comunicaciones_y_Operaciones', 'Manejo de Medios', 'Si los medios que ya no son requeridos son desechados en forma segura según un procedimiento formal.\r\n'),
(31, 'Administración_Comunicaciones_y_Operaciones', 'Manejo de Medios', 'Si existe un procedimiento para manejar el almacenaje de la información. Este procedimiento cubre temas como ser la protección de la información de divulgación no autorizada o mal uso.\r\n'),
(32, 'Administración_Comunicaciones_y_Operaciones', 'Manejo de Medios', 'Si la documentación de sistemas es protegida contra accesos no autorizados.\r\n'),
(33, 'Administración_Comunicaciones_y_Operaciones', 'Intercambio de Información', 'Si se han establecido acuerdos con relación al intercambio de información y software entre la organización y terceros.\r\n'),
(34, 'Administración_Comunicaciones_y_Operaciones', 'Intercambio de Información', 'Si los medios conteniendo información es protegida contra accesos no autorizados, mal uso o corrupción durante su transportación fuera de las instalaciones físicas de la organización.\r\n'),
(35, 'Administración_Comunicaciones_y_Operaciones', 'Servicios de Comercio Electrónico', 'Si la información involucrada en el comercio electrónico que se transmite sobre redes publicas esta protegida de actividades fraudulentas, disputas contractuales y cualquier acceso no autorizado o modificaciones.\r\n'),
(36, 'Administración_Comunicaciones_y_Operaciones', 'Servicios de Comercio Electrónico', 'Si controles de seguridad como la aplicación de controles criptográficos se han tomado en consideración.\r\n'),
(37, 'Administración_Comunicaciones_y_Operaciones', 'Servicios de Comercio Electrónico', 'Si la integridad de información públicamente disponible esta protegida contra modificaciones no autorizadas.\r\n'),
(38, 'Administración_Comunicaciones_y_Operaciones', 'Monitoreo', 'Si se desarrollaron procedimientos para monitorear el uso de sistemas de las facilidades de procesamientos de informática y se refuerza su cumplimiento.\r\n'),
(39, 'Administración_Comunicaciones_y_Operaciones', 'Monitoreo', 'Si los resultados del monitoreo se revisan regularmente.\r\n'),
(40, 'Administración_Comunicaciones_y_Operaciones', 'Monitoreo', 'Si las áreas donde se almacenan los registros/logs y las informaciones de los registros/logs están bien protegidas contra manipulación y accesos no autorizados.\r\n'),
(41, 'Administración_Comunicaciones_y_Operaciones', 'Monitoreo', 'Si las actividades de los Administradores o Operadores son registradas.\r\n'),
(42, 'Administración_Comunicaciones_y_Operaciones', 'Monitoreo', 'Si dichos registros son revisados con regularidad.\r\n'),
(43, 'Administración_de_Incidentes_de_Seguridad_Informática', 'Administración de incidentes y mejoras de Seguridad Informática', 'Si información sobre eventos de seguridad informáticos son reportados a través de canales de administración apropiados tan rápido como es posible.'),
(44, 'Administración_de_Incidentes_de_Seguridad_Informática', 'Administración de incidentes y mejoras de Seguridad Informática\r\n', 'Si un procedimiento formal para reportar eventos de seguridad informática, procedimiento de respuesta y escalacion de Incidentes ha sido desarrollado e implementado.\r\n'),
(45, 'Administración_de_Incidentes_de_Seguridad_Informática', 'Administración de incidentes y mejoras de Seguridad Informática\r\n', 'Si existe un mecanismo para identificar y cuantificar el tipo, volumen y costos de los incidentes de seguridad informáticos.\r\n'),
(46, 'Administración_de_Incidentes_de_Seguridad_Informática', 'Administración de incidentes y mejoras de Seguridad Informática\r\n', 'Si la información obtenida de la evaluación de los incidentes de seguridad informáticos pasados son utilizados para identificar incidentes recurrentes y de alto impacto.\r\n'),
(47, 'Administración_de_Incidentes_de_Seguridad_Informática', 'Administración de incidentes y mejoras de Seguridad Informática\r\n', 'Si una acción de seguimiento contra una persona u organización después de un incidente de seguridad informático involucra acciones legales (ya sea civil o criminal). \r\n'),
(48, 'Administración_Continuidad_de_Negocios', 'Aspectos de Administración Continuidad de Negocios\r\n', 'Si existe un proceso administrado que define los requisitos de seguridad informática para el desarrollo y mantenimiento de la continuidad de negocios a través de la organización.\r\n'),
(49, 'Administración_Continuidad_de_Negocios', 'Aspectos de Administración Continuidad de Negocios\r\n', 'Si los eventos que causan interrupciones a los procesos del negocio han sido identificados junto con la probabilidad e impacto que tales interrupciones y sus consecuencias por la seguridad informática.\r\n'),
(50, 'Administración_Continuidad_de_Negocios', 'Aspectos de Administración Continuidad de Negocios\r\n', 'Si los planes fueron desarrollados para mantener y restaurar las operaciones del negocio, asegurar la disponibilidad de la información dentro de los niveles requeridos en los tiempos requeridos seguidos a una interrupción o falla a los procesos de negocios.\r\n'),
(51, 'Administración_Continuidad_de_Negocios', 'Aspectos de Administración Continuidad de Negocios\r\n', 'Si solo existe una arquitectura para el Plan de Continuidad de Negocios.\r\n'),
(52, 'Administración_Continuidad_de_Negocios', 'Aspectos de Administración Continuidad de Negocios\r\n', 'Si los Planes de Continuidad de Negocios son probados regularmente para garantizar que están actualizados y son efectivos.\r\n'),
(53, 'Cumplimiento', 'Cumplimiento con Regulaciones Legales\r\n', 'Si todos los requisitos relevantes a regulaciones, contratos, etc. y el enfoque organizativo para cumplir los requisitos fueron definidos explícitamente y documentados para cada sistema informático y organización.\r\n'),
(54, 'Cumplimiento', 'Cumplimiento con Regulaciones Legales\r\n', 'Si existen procedimientos que garanticen el cumplimiento con requisitos legislativos, regulaciones y contratos en el uso de material en el cual pueden existir derechos de propiedad intelectual y en el uso de productos de software propietarios.\r\n'),
(55, 'Cumplimiento', 'Cumplimiento con Regulaciones Legales\r\n', 'Si registros importantes de la organización son protegidos de perdidas, destrucción y falsificación de acuerdo con regulaciones, contratos y requisitos de negocios.\r\n'),
(56, 'Cumplimiento', 'Cumplimiento con Regulaciones Legales\r\n', 'Si la protección y privacidad de los datos es asegurada según legislación, regulación relevantes y si es aplicable según cláusulas contractuales.\r\n'),
(57, 'Cumplimiento', 'Cumplimiento con Regulaciones Legales\r\n', 'Si el uso de las facilidades de procesos informáticos para efectos personales o no autorizados sin el consentimiento de la gerencia es tratado como uso inapropiado de las facilidades.\r\n'),
(58, 'Cumplimiento', 'Cumplimiento con Regulaciones Legales\r\n', 'Si los controles criptográficos son utilizados en cumplimiento con acuerdo, leyes y regulaciones relevantes.\r\n'),
(59, 'Cumplimiento', 'Cumplimiento con Políticas y estándares de Seguridad y cumplimientos técnicos\r\n', 'Si los gerentes aseguran que todos los procedimientos de seguridad dentro de sus áreas de responsabilidad son efectuados correctamente para lograr cumplimiento con las políticas y estándares de seguridad.\r\n'),
(60, 'Cumplimiento', 'Cumplimiento con Políticas y estándares de Seguridad y cumplimientos técnicos\r\n\r\n', 'Si los sistemas informáticos son regularmente revisados con relación al cumplimiento con la implementación de estándares de seguridad.\r\n'),
(61, 'Cumplimiento', 'Consideraciones Auditorias de Sistemas\r\n\r\n', 'Si los requisitos de auditoria y actividades que involucran revisiones de los sistemas operacionales deben ser planeadas cuidadosamente y acordadas para minimizar los riesgos o interrupciones a los procesos de negocio.\r\n'),
(62, 'Cumplimiento', 'Consideraciones Auditorias de Sistemas\r\n\r\n', 'Si acceso a las herramientas de auditoria de sistemas como ser software o archivos de datos son protegidos para prevenir posible mal uso o compromisos.\r\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prueba`
--

DROP TABLE IF EXISTS `prueba`;
CREATE TABLE IF NOT EXISTS `prueba` (
  `rut` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `p1` int(1) DEFAULT NULL,
  `p2` int(1) DEFAULT NULL,
  `p3` int(1) DEFAULT NULL,
  `p4` int(1) DEFAULT NULL,
  `p5` int(1) DEFAULT NULL,
  `p6` int(1) DEFAULT NULL,
  `p7` int(1) DEFAULT NULL,
  `p8` int(1) DEFAULT NULL,
  `p9` int(1) DEFAULT NULL,
  `p10` int(1) DEFAULT NULL,
  `p11` int(1) DEFAULT NULL,
  `p12` int(1) DEFAULT NULL,
  `p13` int(1) DEFAULT NULL,
  `p14` int(1) DEFAULT NULL,
  `p15` int(1) DEFAULT NULL,
  `p16` int(1) DEFAULT NULL,
  `p17` int(1) DEFAULT NULL,
  `p18` int(1) DEFAULT NULL,
  `p19` int(1) DEFAULT NULL,
  `p20` int(1) DEFAULT NULL,
  `p21` int(1) DEFAULT NULL,
  `p22` int(1) DEFAULT NULL,
  `p23` int(1) DEFAULT NULL,
  `p24` int(1) DEFAULT NULL,
  `p25` int(1) DEFAULT NULL,
  `p26` int(1) DEFAULT NULL,
  `p27` int(1) DEFAULT NULL,
  `p28` int(1) DEFAULT NULL,
  `p29` int(1) DEFAULT NULL,
  `p30` int(1) DEFAULT NULL,
  `p31` int(1) DEFAULT NULL,
  `p32` int(1) DEFAULT NULL,
  `p33` int(1) DEFAULT NULL,
  `p34` int(1) DEFAULT NULL,
  `p35` int(1) DEFAULT NULL,
  `p36` int(1) DEFAULT NULL,
  `p37` int(1) DEFAULT NULL,
  `p38` int(1) DEFAULT NULL,
  `p39` int(1) DEFAULT NULL,
  `p40` int(1) DEFAULT NULL,
  `p41` int(1) DEFAULT NULL,
  `p42` int(1) DEFAULT NULL,
  `p43` int(1) DEFAULT NULL,
  `p44` int(1) DEFAULT NULL,
  `p45` int(1) DEFAULT NULL,
  `p46` int(1) DEFAULT NULL,
  `p47` int(1) DEFAULT NULL,
  `p48` int(1) DEFAULT NULL,
  `p49` int(1) DEFAULT NULL,
  `p50` int(1) DEFAULT NULL,
  `p51` int(1) DEFAULT NULL,
  `p52` int(1) DEFAULT NULL,
  `p53` int(1) DEFAULT NULL,
  `p54` int(1) DEFAULT NULL,
  `p55` int(1) DEFAULT NULL,
  `p56` int(1) DEFAULT NULL,
  `p57` int(1) DEFAULT NULL,
  `p58` int(1) DEFAULT NULL,
  `p59` int(1) DEFAULT NULL,
  `p60` int(1) DEFAULT NULL,
  `p61` int(1) DEFAULT NULL,
  `p62` int(1) DEFAULT NULL,
  PRIMARY KEY (`rut`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `prueba`
--

INSERT INTO `prueba` (`rut`, `p1`, `p2`, `p3`, `p4`, `p5`, `p6`, `p7`, `p8`, `p9`, `p10`, `p11`, `p12`, `p13`, `p14`, `p15`, `p16`, `p17`, `p18`, `p19`, `p20`, `p21`, `p22`, `p23`, `p24`, `p25`, `p26`, `p27`, `p28`, `p29`, `p30`, `p31`, `p32`, `p33`, `p34`, `p35`, `p36`, `p37`, `p38`, `p39`, `p40`, `p41`, `p42`, `p43`, `p44`, `p45`, `p46`, `p47`, `p48`, `p49`, `p50`, `p51`, `p52`, `p53`, `p54`, `p55`, `p56`, `p57`, `p58`, `p59`, `p60`, `p61`, `p62`) VALUES
('185401774', 1, 0, 0, 1, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2),
('189334710', 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `respuestas`
--

DROP TABLE IF EXISTS `respuestas`;
CREATE TABLE IF NOT EXISTS `respuestas` (
  `rut` int(15) NOT NULL,
  `p1` int(1) NOT NULL,
  `p2` int(1) NOT NULL,
  `p3` int(1) NOT NULL,
  `p4` int(1) NOT NULL,
  `p5` int(1) NOT NULL,
  `p6` int(1) NOT NULL,
  `p7` int(1) NOT NULL,
  `p8` int(1) NOT NULL,
  `p9` int(1) NOT NULL,
  `p10` int(1) NOT NULL,
  `p11` int(1) NOT NULL,
  `p12` int(1) NOT NULL,
  `p13` int(1) NOT NULL,
  `p14` int(1) NOT NULL,
  `p15` int(1) NOT NULL,
  `p16` int(1) NOT NULL,
  `p17` int(1) NOT NULL,
  `p18` int(1) NOT NULL,
  `p19` int(1) NOT NULL,
  `p20` int(1) NOT NULL,
  `p21` int(1) NOT NULL,
  `p22` int(1) NOT NULL,
  `p23` int(1) NOT NULL,
  `p24` int(1) NOT NULL,
  `p25` int(1) NOT NULL,
  `p26` int(1) NOT NULL,
  `p27` int(1) NOT NULL,
  `p28` int(1) NOT NULL,
  `p29` int(1) NOT NULL,
  `p30` int(1) NOT NULL,
  `p31` int(1) NOT NULL,
  `p32` int(1) NOT NULL,
  `p33` int(1) NOT NULL,
  `p34` int(1) NOT NULL,
  `p35` int(1) NOT NULL,
  `p36` int(1) NOT NULL,
  `p37` int(1) NOT NULL,
  `p38` int(1) NOT NULL,
  `p39` int(1) NOT NULL,
  `p40` int(1) NOT NULL,
  `p41` int(1) NOT NULL,
  `p42` int(1) NOT NULL,
  `p43` int(1) NOT NULL,
  `p44` int(1) NOT NULL,
  `p45` int(1) NOT NULL,
  `p46` int(1) NOT NULL,
  `p47` int(1) NOT NULL,
  `p48` int(1) NOT NULL,
  `p49` int(1) NOT NULL,
  `p50` int(1) NOT NULL,
  `p51` int(1) NOT NULL,
  `p52` int(1) NOT NULL,
  `p53` int(1) NOT NULL,
  `p54` int(1) NOT NULL,
  `p55` int(1) NOT NULL,
  `p56` int(1) NOT NULL,
  `p57` int(1) NOT NULL,
  `p58` int(1) NOT NULL,
  `p59` int(1) NOT NULL,
  `p60` int(1) NOT NULL,
  `p61` int(1) NOT NULL,
  `p62` int(1) NOT NULL,
  `p63` int(1) NOT NULL,
  `p64` int(1) NOT NULL,
  `p65` int(1) NOT NULL,
  `p66` int(1) NOT NULL,
  `p67` int(1) NOT NULL,
  `p68` int(1) NOT NULL,
  `p69` int(1) NOT NULL,
  `p70` int(1) NOT NULL,
  `p71` int(1) NOT NULL,
  `p72` int(1) NOT NULL,
  `p73` int(1) NOT NULL,
  `p74` int(1) NOT NULL,
  `p75` int(1) NOT NULL,
  `p76` int(1) NOT NULL,
  `p77` int(1) NOT NULL,
  `p78` int(1) NOT NULL,
  `p79` int(1) NOT NULL,
  `p80` int(1) NOT NULL,
  `p81` int(1) NOT NULL,
  `p82` int(1) NOT NULL,
  `p83` int(1) NOT NULL,
  `p84` int(1) NOT NULL,
  `p85` int(1) NOT NULL,
  `p86` int(1) NOT NULL,
  `p87` int(1) NOT NULL,
  `p88` int(1) NOT NULL,
  `p89` int(1) NOT NULL,
  `p90` int(1) NOT NULL,
  `p91` int(1) NOT NULL,
  `p92` int(1) NOT NULL,
  `p93` int(1) NOT NULL,
  `p94` int(1) NOT NULL,
  `p95` int(1) NOT NULL,
  `p96` int(1) NOT NULL,
  `p97` int(1) NOT NULL,
  `p98` int(1) NOT NULL,
  `p99` int(1) NOT NULL,
  `p100` int(1) NOT NULL,
  `p101` int(1) NOT NULL,
  `p102` int(1) NOT NULL,
  `p103` int(1) NOT NULL,
  `p104` int(1) NOT NULL,
  `p105` int(1) NOT NULL,
  `p106` int(1) NOT NULL,
  `p107` int(1) NOT NULL,
  `p108` int(1) NOT NULL,
  `p109` int(1) NOT NULL,
  `p110` int(1) NOT NULL,
  `p111` int(1) NOT NULL,
  `p112` int(1) NOT NULL,
  `p113` int(1) NOT NULL,
  `p114` int(1) NOT NULL,
  `p115` int(1) NOT NULL,
  `p116` int(1) NOT NULL,
  `p117` int(1) NOT NULL,
  `p118` int(1) NOT NULL,
  `p119` int(1) NOT NULL,
  `p120` int(1) NOT NULL,
  PRIMARY KEY (`rut`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `nickname` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `name` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `lastname` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `rut` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `phone` int(12) NOT NULL,
  `email` varchar(60) COLLATE utf8_spanish_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  `address` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`nickname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`nickname`, `name`, `lastname`, `rut`, `phone`, `email`, `gender`, `address`, `password`) VALUES
('Godmachine', 'Gustavo', 'Vega', '185401774', 932271578, 'mechagymgm@gmail.com', 'hombre', 'Riojana 160', 'THEGOD101a#'),
('Arehudson', 'Genesis', 'Arellano', '261202697', 96441578, 'are.hudson.28@gmail.com', 'hombre', 'Buin 120', 'THEGOD101a#'),
('Gustavito', 'asdf', 'asdf', '18540177', 12345, 'mechasito@as.com', 'hombre', 'asdf', '12345'),
('Juampi28', 'Juan', 'Mellado', '189334710', 96447945, 'juanpi@gmail.com', 'hombre', 'buin 24', 'ASDF1234');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_empresa`
--

DROP TABLE IF EXISTS `usuario_empresa`;
CREATE TABLE IF NOT EXISTS `usuario_empresa` (
  `rute` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `empresa` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `cargo` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`rute`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario_empresa`
--

INSERT INTO `usuario_empresa` (`rute`, `empresa`, `cargo`) VALUES
('185401774', 'Forestal', 'Desarrollador'),
('189334710', 'Forestal', 'Diseñador'),
('261202697', 'Forestal', 'Relacionadora publica');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
