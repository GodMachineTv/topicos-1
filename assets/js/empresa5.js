function inicio()
{
    $("#colador").append("<br><div id='validar' style='text-align:center;padding:10px;'><a href='#' id='dominios_res' style='text-decoration:none;font-family:century gothic;'>Validar <i class='fa fa-user'></i></a></div>");
    
    $.ajax(
    {
        url: '../controlador/6.1 listar_empresas.php',
        type: 'POST',
        data: {}, 
        dataType: 'json',
    })
    
    .done(function(data) 
    {
        $("#empresas_s").append("<option selected value='0'>------</option>");

        data.forEach(function(item)
        {
           $("#empresas_s").append("<option value='" + item['nombre'] + "'>" + item['nombre'] + "</option>");
        }); 
    })

    .fail(function() 
    {
        alert("Error al listar!");
    });    
}

$(document).ready(function(){
    
    $('body').on('click', '#dominios_res', function()
    {
        if(document.getElementById("finish"))
        {
            imagen = document.getElementById('finish');
            padre = imagen.parentNode;
            padre.removeChild(imagen);
        }



        var userx = document.getElementById('margen');
        var clase = userx.getAttribute('class');
        var select = document.getElementById('empresas_s').value;
        var userx = document.getElementById('empresas_in').value;
        bandera=0;
        
        $.ajax(
        {
            url: '../controlador/6.3 valida rut.php',
            type: 'POST',
            data: {'rut':userx}, 
            dataType: 'json',
        })
    
        .done(function(data) 
        {
            if(select=='0')
            {
                imagen = document.getElementById('borradors');
                padre = imagen.parentNode;
		        padre.removeChild(imagen);
                
                $("#cuerpo_preguntas").append("<div id='borradors'><span id='borrador_span'>¡Error! <img src='../assets/images/Error.png'></span><br><br><span id='borrador_span2'>No ha seleccionado una empresa!<br><br>Para confirmar su rut es necesario seleccionar una empresa</span><br><br><hr></div>");
            }
            else
            {
                imagen = document.getElementById('borradors');
                padre = imagen.parentNode;
		        padre.removeChild(imagen);
                
                $("#cuerpo_preguntas").append("<div id='borradors'><span id='borrador_span'>Empresa seleccionada <img src='../assets/images/Comprobado.png'></span><br><br><hr></div>");
            }
   
            if(data==0 || data=='0')
            {
                imagen = document.getElementById('borrador');
                padre = imagen.parentNode;
		        padre.removeChild(imagen);
                
                $("#cuerpo_preguntas").append("<div id='borrador'><span id='borrador_span'>¡Error! <img src='../assets/images/Error.png'></span><br><br><span id='borrador_span2'>El rut ingresado es invalido o no pertenece a la empresa. <br><br>Solo usuarios pertenecientes a la empresa pueden tener acceso a esta informacion.</span></div>");
                
            }
            if(data>0)
            {
                imagen = document.getElementById('borrador');
                padre = imagen.parentNode;
		        padre.removeChild(imagen);
                
                $("#cuerpo_preguntas").append("<div id='borrador'><span id='borrador_span'>¡Comprobado! <img src='../assets/images/Comprobado.png'></span><br><br><span id='borrador_span2'>El rut ingresado existe.<br><br>A continuacion se comprobara si es parte de la organizacion.</span></div>");
            }
            if(data<0)
            {
                imagen = document.getElementById('borrador');
                padre = imagen.parentNode;
		        padre.removeChild(imagen);
                
                $("#cuerpo_preguntas").append("<div id='borrador'><span id='borrador_span'>¡Error! <img src='../assets/images/Error.png'></span><br><br><span id='borrador_span2'>El campo de confirmacion de rut esta vacío! <br><br>Ingrese su rut para su verificacion.</span></div>");
            } 

            if(data>0 && select!=0)
            {                
                $.ajax(
                {
                    url: '../controlador/6.2 valida_empresa.php',
                    type: 'POST',
                    data: {'rut':userx, 'empresa':select, 'persona':data}, 
                    dataType: 'json',
                })
    
                .done(function(data) 
                {
                    //alert(data);
                    //alert(userx);
                    if(data==0 || data=='0')
                    {
                        imagen = document.getElementById('borrador');
                        padre = imagen.parentNode;
                        padre.removeChild(imagen);

                        $("#cuerpo_preguntas").append("<div id='borrador'><span id='borrador_span'>¡Error!, el Rut ingresado no pertenece a la empresa! <img src='../assets/images/Error.png'></span><br><br><hr></div>");                  
                    }
                    else
                    {

                        imagen = document.getElementById('borrador');
                        padre = imagen.parentNode;
                        padre.removeChild(imagen);

                        $("#cuerpo_preguntas").append("<div id='borrador'><span id='borrador_span'>¡Rut aprobado! <img src='../assets/images/Comprobado.png'></span><br><br><hr></div>");
                    
                    
                    
                        $.ajax(
                        {
                            url: '../controlador/6.4 listar_empresa.php',
                            type: 'POST',
                            data: {'id':clase, 'empresa':select, 'persona':data}, 
                            dataType: 'json',
                        })
    
                        .done(function(data) 
                        {
                            var total2;
                            var i;
                            var j=(data.length/2);
                            var total=0;
                            var copia=[];
                            var copia=data;
                            var auxi;
                        
        
                            $("#resultado_empresa").append("<div id='finish' style='margin-left:auto;margin-right:auto;width:700px;background:#fff;color:black;font-family:century gothic;border-radius:10px;'><h2 style='text-align:center;padding-top:10px;padding-bottom:10px;'>Resulltados generales de empresa</h2><br><hr><br></div>");
                        
                            for(i=0; i<data.length; i++)
                            {
                                if(i<(data.length/2))
                                {
                                    total=Math.round((data[i]/(data[i]+data[j]))*100);
    
                                    if(total<33)
                                    {
                                        $("#finish").append("<br><div id='titus" + i + "'></div><br><table id='tabla_dominio' style='text-align:center; width:95%;padding-left:10px;padding-right:10px;margin-left:auto;margin-right:auto;'><tr><td id='dom1'><b>Resultado</b></td><td id='dom2'><b>Buenas</b></td><td id='dom3'><b>Malas</b></td><td id='dom4'><b>Recomendacion</b></td></tr><tr><td id='doms1'>Intermedio: <img id='imgs' src='../assets/images/Intermedio.png'> </td><td id='doms2'>" + data[i] + " <img id='imgs' src='../assets/images/Comprobado.png'></td><td id='doms3'>" + data[j] + " <img id='imgs' src='../assets/images/Error.png'></td><td id='dom5' class='reco" + i + "'></td></tr><tr><td id='coladom'><t/d><td id='coladom'></td><td id='coladom'></td><td id='coladom'></td></tr></table><br><h3 style='text-align:center;padding-top:10px;padding-bottom:10px;'>Gráficas</h3><table id='graficas'><tr><td id='gra1'><b>Respuesta</b></td><td id='gra1espacio'></td><td id='gra1f'></td><td id='gra1f'></td></tr><tr><td id='gra2' class='ps" + i + "'></td><td id='gra2barra' class='gb2" + i + "'></td><td id='gra2final'></td><td id='gra2f'></td></tr><tr><td id='gra22' class='pss" + i + "'></td><td id='gra2barra2' class='gb22" + i + "'></td><td id='gra2final2'></td><td id='gra2f2'></td></tr><tr><td id='gra2' class='psss" + i + "'></td><td id='gra2barra' class='gb222" + i + "'></td><td id='gra2final'></td><td id='gra2f'></td></tr><tr><td id='gra3' class='gra3" + i + "'></td><td id='gra3l' class='gra3l" + i + "'></td><td id='gra3r' class='gra3r" + i + "'></td><td id='gra3por'><b>%</b></td></tr></table><br><br><hr><hr><br>");     
                                    }
                                    if(total>=66)
                                    {
                                        $("#finish").append("<br><div id='titus" + i + "'></div><br><table id='tabla_dominio' style='text-align:center; width:95%;padding-left:10px;padding-right:10px;margin-left:auto;margin-right:auto;'><tr><td id='dom1'><b>Resultado</b></td><td id='dom2'><b>Buenas</b></td><td id='dom3'><b>Malas</b></td><td id='dom4'><b>Recomendacion</b></td></tr><tr><td id='doms1'>Intermedio: <img id='imgs' src='../assets/images/Intermedio.png'> </td><td id='doms2'>" + data[i] + " <img id='imgs' src='../assets/images/Comprobado.png'></td><td id='doms3'>" + data[j] + " <img id='imgs' src='../assets/images/Error.png'></td><td id='dom5' class='reco" + i + "'></td></tr><tr><td id='coladom'><t/d><td id='coladom'></td><td id='coladom'></td><td id='coladom'></td></tr></table><br><h3 style='text-align:center;padding-top:10px;padding-bottom:10px;'>Gráficas</h3><table id='graficas'><tr><td id='gra1'><b>Respuesta</b></td><td id='gra1espacio'></td><td id='gra1f'></td><td id='gra1f'></td></tr><tr><td id='gra2' class='ps" + i + "'></td><td id='gra2barra' class='gb2" + i + "'></td><td id='gra2final'></td><td id='gra2f'></td></tr><tr><td id='gra22' class='pss" + i + "'></td><td id='gra2barra2' class='gb22" + i + "'></td><td id='gra2final2'></td><td id='gra2f2'></td></tr><tr><td id='gra2' class='psss" + i + "'></td><td id='gra2barra' class='gb222" + i + "'></td><td id='gra2final'></td><td id='gra2f'></td></tr><tr><td id='gra3' class='gra3" + i + "'></td><td id='gra3l' class='gra3l" + i + "'></td><td id='gra3r' class='gra3r" + i + "'></td><td id='gra3por'><b>%</b></td></tr></table><br><br><hr><hr><br>");     
                                    }
                                    if(total>=33 && total<66)
                                    {
                                        $("#finish").append("<br><div id='titus" + i + "'></div><br><table id='tabla_dominio' style='text-align:center; width:95%;padding-left:10px;padding-right:10px;margin-left:auto;margin-right:auto;'><tr><td id='dom1'><b>Resultado</b></td><td id='dom2'><b>Buenas</b></td><td id='dom3'><b>Malas</b></td><td id='dom4'><b>Recomendacion</b></td></tr><tr><td id='doms1'>Intermedio: <img id='imgs' src='../assets/images/Intermedio.png'> </td><td id='doms2'>" + data[i] + " <img id='imgs' src='../assets/images/Comprobado.png'></td><td id='doms3'>" + data[j] + " <img id='imgs' src='../assets/images/Error.png'></td><td id='dom5' class='reco" + i + "'></td></tr><tr><td id='coladom'><t/d><td id='coladom'></td><td id='coladom'></td><td id='coladom'></td></tr></table><br><h3 style='text-align:center;padding-top:10px;padding-bottom:10px;'>Gráficas</h3><table id='graficas'><tr><td id='gra1'><b>Respuesta</b></td><td id='gra1espacio'></td><td id='gra1f'></td><td id='gra1f'></td></tr><tr><td id='gra2' class='ps" + i + "'></td><td id='gra2barra' class='gb2" + i + "'></td><td id='gra2final'></td><td id='gra2f'></td></tr><tr><td id='gra22' class='pss" + i + "'></td><td id='gra2barra2' class='gb22" + i + "'></td><td id='gra2final2'></td><td id='gra2f2'></td></tr><tr><td id='gra2' class='psss" + i + "'></td><td id='gra2barra' class='gb222" + i + "'></td><td id='gra2final'></td><td id='gra2f'></td></tr><tr><td id='gra3' class='gra3" + i + "'></td><td id='gra3l' class='gra3l" + i + "'></td><td id='gra3r' class='gra3r" + i + "'></td><td id='gra3por'><b>%</b></td></tr></table><br><br><hr><hr><br>");                            
                                    }
                                
                                
                                    if(copia[i]>copia[j])
                                    {
                                        total=total;
                                        auxi=".gb2".concat(i);
                                        $(auxi).append("<hr id='pico' style='background:#56BE18;height:13px;width:" + total*5 + "px;border: 2px solid #000000;text-align:left;margin-left:0px;'> <span id='csa' >" + total + "%</span>");
                                        
                                        total=Math.round((data[j]/(data[i]+data[j]))*100);
                                        total=total;
                                        auxi=".gb22".concat(i);
                                        $(auxi).append("<hr id='pico' style='background:#ff0000;height:13px;width:" + total*5 + "px;border: 2px solid #000000;text-align:left;margin-left:0px;'> <span id='csa' >" + total + "%</span>");
                                    
                                        total=100;
                                        auxi=".gb222".concat(i);
                                        $(auxi).append("<hr id='pico' style='background:#1894BE;height:13px;width:" + total*5 + "px;border: 2px solid #000000;text-align:left;margin-left:0px;'> <span id='csa' >" + total + "%</span>");
                                    
                                        total=".ps".concat(i);
                                        $(total).append("<img src='../assets/images/Comprobado.png'>  " + data[i] + "");
                                        total=".pss".concat(i);
                                        $(total).append("<img src='../assets/images/Error.png'>  " + data[j] + "");
                                        total=".psss".concat(i);
                                        $(total).append("<img src='../assets/images/Full.png'>   " + (data[i]+data[j]) + "");
                                    
                                        total=".gra3".concat(i);
                                        $(total).append('<b>0</b>');
                                        total=".gra3l".concat(i);
                                        $(total).append('<b>1</b>');
                                        total=".gra3r".concat(i);
                                        $(total).append("<b>" + 100 + "</b>");
                                    
                                    //
                                    }
                                    else
                                    {
                                        total=Math.round((data[i]/(data[i]+data[j]))*100);
                                        total=total;
                                        auxi=".gb22".concat(i);
                                        $(auxi).append("<hr id='pico' style='background:#56BE18;height:13px;width:" + total*5 + "px;border: 2px solid #000000;text-align:left;margin-left:0px;'> <span id='csa' style='text-align:left;margin-left:0px;'>" + total + "%</span>");
                                    
                                        total=Math.round((data[j]/(data[i]+data[j]))*100);
                                        total=total;
                                        auxi=".gb2".concat(i);
                                        $(auxi).append("<hr id='pico' style='background:#ff0000;height:13px;width:" + total*5 + "px;border: 2px solid #000000;text-align:left;margin-left:0px;'> <span id='csa' >" + total + "%</span>");
                                    
                                        total=100;
                                        auxi=".gb222".concat(i);
                                        $(auxi).append("<hr id='pico' style='background:#1894BE;height:13px;width:" + total*5 + "px;border: 2px solid #000000;text-align:left;margin-left:0px;'> <span id='csa' style='text-align:left;margin-left:0px;'>" + total + "%</span>");
                                        
                                        total=".pss".concat(i);
                                        $(total).append("<img src='../assets/images/Comprobado.png'>  " + data[i] + "");
                                        total=".ps".concat(i);
                                        $(total).append("<img src='../assets/images/Error.png'>  " + data[j] + "");
                                        total=".psss".concat(i);
                                        $(total).append("<img src='../assets/images/Full.png'>   " + (data[i]+data[j]) + "");
                                    
                                        total=".gra3".concat(i);
                                        $(total).append('<b>0</b>');
                                        total=".gra3l".concat(i);
                                        $(total).append('<b>1</b>');
                                        total=".gra3r".concat(i);
                                        $(total).append("<b>" + 100 + "</b>");
                                    }
                                
                                    j++;
                                }
                                else
                                {
                                    break;
                                }
                            }
                        
                            $.ajax(
                            {
                                url: '../controlador/6.5 listar_faltante.php',
                                type: 'POST',
                                data: {'id':clase}, 
                                dataType: 'json',
                            })
    
                            .done(function(data) 
                            {
                                var i=0;
                                var j=copia.length/2;
                                var total;
                            
                            
                                data.forEach(function(item)
                                {

                                    total=Math.round((copia[i]/(copia[i]+copia[j]))*100);
                                    
                                    if(total>=66)
                                    {
                                        total="#titus".concat(i);
                                        $(total).append("<h2 style='text-align:center;padding-bottom:10px;'>" + item["sector"] + "</h2>");
                                        total=".reco".concat(i);
                                        $(total).append("¡Felicitaciones, su resultado es excelente, siga aspirando a la excelencia!");
                                    
                                    }
                                    if(total<33)
                                    {
                                        total="#titus".concat(i);
                                        $(total).append("<h2 style='text-align:center;padding-bottom:10px;'>" + item["sector"] + "</h2>");
                                        total=".reco".concat(i);
                                        $(total).append(item["conclusion"]);
                                    }
                                    if(total>=33 && total<66)
                                    {
                                        total="#titus".concat(i);
                                        $(total).append("<h2 style='text-align:center;padding-bottom:10px;'>" + item["sector"] + "</h2>");
                                        total=".reco".concat(i);
                                        $(total).append(item["conclusion2"]);
                                    }
                                
                                    i++;
                                    j++;
                                
                                });
                            })
                    
                            .fail(function() 
                            {
                                alert("Error al listar!");
                            });
               
                        })

                        .fail(function() 
                        {
                            alert("Error al listar!");
                        });
                    
                    }
                })

                .fail(function() 
                {
                    alert("Error al listar!");
                });
            }
        })

        .fail(function() 
        {
            alert("Error al listar!");
        });
    });
    
});
