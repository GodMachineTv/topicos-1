<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Encuesta de Seguridad</title>

      <link rel="stylesheet" href="../assets/css/exito_login.css">
      <link rel="stylesheet" href="../assets/css/font-awesome.min.css">
      <script src="../assets/js/jquery-3.2.1.js"></script>

    </head>
    
    <body>

        <img id="fondo" src="../assets/images/fondo.png" alt="background image" />

        <div class="container">
            <div class="wrapper">

                <ul class="steps">
                    <li class="is-active">Autenticacion exitosa!</li>
                </ul>

                <form action="#" method="post" class="form-wrapper">

                    <fieldset class="section is-active">
                        <h3>Bienvenido Sr. <?php echo($id); ?>!</h3>

                        <div class = "mensaje7" id="mensaje7">
                            <img src="../assets/images/exito_login.gif" class= "exito" id="exito" />
                            <span> Para continuar seleccione ingresar </span>
                        </div>
               
                        <div class="atras2"><a href="../controlador/2 menu.php?usuario=<?php echo($id); ?>">Ingresar</a></div>
                        <!--<div class="atras2">Volver</div>-->      
                    </fieldset>
                                
                </form>
            </div>
        </div>

    </body>

</html>