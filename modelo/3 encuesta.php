<?php

class encuesta
    {
        private $db;
        private $pasaje;

        public function __construct()
        {
            require_once("../modelo/conectar_bd.php");

            $this->db=conectar_bd::conexion();

            $this->pasaje=array();
        }

        public function get_encuesta()
        {
            $id=@$_POST['id'];
            $id=rtrim($id);

            $consulta=$this->db->query("SELECT numero, sector, sub, preguntas FROM preguntas;");

            while($filas=$consulta->fetch(PDO::FETCH_ASSOC))
            {
                $this->pasaje[]=$filas;
            }

            return $this->pasaje;            
        }
    
        public function get_encuesta_guardada($ruta)
        {
            $id=@$_POST['id'];
            $id=rtrim($id);
            
            $rut;
            
            foreach($ruta as $sub):
                $rut=$sub["rut"];
            endforeach;

            $consulta=$this->db->query("SELECT p.numero, p.sector, p.sub, p.preguntas, r.p1, r.p2, r.p3, r.p4, r.p5, r.p6, r.p7, r.p8, r.p9, r.p10, r.p11, r.p12, r.p13, r.p14, r.p15, r.p16, r.p17, r.p18, r.p19, r.p20, r.p21, r.p22, r.p23, r.p24, r.p25, r.p26, r.p27, r.p28, r.p29, r.p30, r.p31, r.p32, r.p33, r.p34, r.p35, r.p36, r.p37, r.p38, r.p39, r.p40, r.p41, r.p42, r.p43, r.p44, r.p45, r.p46, r.p47, r.p48, r.p49, r.p50, r.p51, r.p52, r.p53, r.p54, r.p55, r.p56, r.p57, r.p58, r.p59, r.p60, r.p61, r.p62 FROM preguntas p, prueba r WHERE r.rut='$rut';");

            while($filas=$consulta->fetch(PDO::FETCH_ASSOC))
            {
                $this->pasaje[]=$filas;
            }

            return $this->pasaje;            
        }
    
        public function get_rut()
        {
            $id=@$_POST['id'];
            $id=rtrim($id);

            $consulta=$this->db->query("SELECT rut FROM usuarios WHERE nickname='$id';");

            while($filas=$consulta->fetch(PDO::FETCH_ASSOC))
            {
                $this->pasaje[]=$filas;
            }

            return $this->pasaje;            
        }
    
        public function get_existe_encuesta()
        {
            $id=@$_POST['id'];
            $id=rtrim($id);
            $t=0;
            
            $consulta=$this->db->query("SELECT p.rut FROM prueba p, usuarios u WHERE p.rut=u.rut and nickname='$id';");

            while($filas=$consulta->fetch(PDO::FETCH_ASSOC))
            {
                $t++;
                $this->pasaje[]=$filas;
            }
            
            return $t;
        }
    
        public function set_encuesta_vacia($ruta)
        {
            $id=@$_POST['id'];
            $id=rtrim($id);
            
            $rut;
            
            foreach($ruta as $sub):
                $rut=$sub["rut"];
            endforeach;
            
            $num=2;
            
            $sql="INSERT INTO prueba (rut, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, p28, p29, p30, p31, p32, p33, p34, p35, p36, p37, p38, p39, p40, p41, p42, p43, p44, p45, p46, p47, p48, p49, p50, p51, p52, p53, p54, p55, p56, p57, p58, p59, p60, p61, p62) VALUES (:rut, :p1, :p2, :p3, :p4, :p5, :p6, :p7, :p8, :p9, :p10, :p11, :p12, :p13, :p14, :p15, :p16, :p17, :p18, :p19, :p20, :p21, :p22, :p23, :p24, :p25, :p26, :p27, :p28, :p29, :p30, :p31, :p32, :p33, :p34, :p35, :p36, :p37, :p38, :p39, :p50, :p51, :p52, :p53, :p54, :p55, :p56, :p57, :p58, :p59, :p50, :p51, :p52, :p53, :p54, :p55, :p56, :p57, :p58, :p59, :p60, :p61, :p62)";

            $resultado=$this->db->prepare($sql);
            $resultado->execute(array(":rut"=>$rut, ":p1"=>$num, ":p2"=>$num, ":p3"=>$num, ":p4"=>$num, ":p5"=>$num, ":p6"=>$num, ":p7"=>$num, ":p8"=>$num, ":p9"=>$num, ":p10"=>$num, ":p11"=>$num, ":p12"=>$num, ":p13"=>$num, ":p14"=>$num, ":p15"=>$num, ":p16"=>$num, ":p17"=>$num, ":p18"=>$num, ":p19"=>$num, ":p20"=>$num, ":p21"=>$num, ":p22"=>$num, ":p23"=>$num, ":p24"=>$num, ":p25"=>$num, ":p26"=>$num, ":p27"=>$num, ":p28"=>$num, ":p29"=>$num, ":p30"=>$num, ":p31"=>$num, ":p32"=>$num, ":p33"=>$num, ":p34"=>$num, ":p35"=>$num, ":p36"=>$num, ":p37"=>$num, ":p38"=>$num, ":p39"=>$num, ":p40"=>$num, ":p41"=>$num, ":p42"=>$num, ":p43"=>$num, ":p44"=>$num, ":p45"=>$num, ":p46"=>$num, ":p47"=>$num, ":p48"=>$num, ":p49"=>$num, ":p50"=>$num, ":p51"=>$num, ":p52"=>$num, ":p53"=>$num, ":p54"=>$num, ":p55"=>$num, ":p56"=>$num, ":p57"=>$num, ":p58"=>$num, ":p59"=>$num, ":p60"=>$num, ":p61"=>$num, ":p62"=>$num));

        }
    
        public function get_respuesta_individual($ruta)
        {          
            $sec=@$_POST['num'];
            $sec=rtrim($sec);
            
            $sec=('p'.$sec);
            
            $rut;

            foreach($ruta as $sub):
                $rut=$sub["rut"];
            endforeach;

            $consulta=$this->db->query("SELECT ".$sec." FROM prueba WHERE rut='$rut';");

            while($filas=$consulta->fetch(PDO::FETCH_ASSOC))
            {
                $this->pasaje[]=$filas;
            }

            return $this->pasaje; 
        }
    
        public function set_respuesta($ruta)
        {
            $id=@$_POST['id'];
            $id=rtrim($id);
            
            $sec=@$_POST['num'];
            $sec=rtrim($sec);
            
            $val=@$_POST['val'];
            $val=rtrim($val);
                       
            $aux=("p" . $sec);
             
            $rut;

            foreach($ruta as $sub):
                $rut=$sub["rut"];
            endforeach;

            $sql="UPDATE prueba SET ". $aux . "=:mivalor WHERE rut=:mirut";

            $resultado=$this->db->prepare($sql);

            $resultado->execute(array(":mirut"=>$rut, ":mivalor"=>$val));
        }
    
        public function get_dominio()
        {          
            $sec=@$_POST['num'];
            $sec=rtrim($sec);

            $consulta=$this->db->query("SELECT sector FROM preguntas WHERE numero='$sec';");

            while($filas=$consulta->fetch(PDO::FETCH_ASSOC))
            {
                $this->pasaje[]=$filas;
            }

            return $this->pasaje; 
        }
    
        public function get_conclusion_individual($ruta)
        {
            $id=@$_POST['id'];
            $id=rtrim($id);
            
            $rut;

            foreach($ruta as $sub):
                $rut=$sub["rut"];
            endforeach;

            $consulta=$this->db->query("SELECT p.numero, p.sector, p.sub, p.preguntas, r.p1, r.p2, r.p3, r.p4, r.p5, r.p6, r.p7, r.p8, r.p9, r.p10, r.p11, r.p12, r.p13, r.p14, r.p15, r.p16, r.p17, r.p18, r.p19, r.p20, r.p21, r.p22, r.p23, r.p24, r.p25, r.p26, r.p27, r.p28, r.p29, r.p30, r.p31, r.p32, r.p33, r.p34, r.p35, r.p36, r.p37, r.p38, r.p39, r.p40, r.p41, r.p42, r.p43, r.p44, r.p45, r.p46, r.p47, r.p48, r.p49, r.p50, r.p51, r.p52, r.p53, r.p54, r.p55, r.p56, r.p57, r.p58, r.p59, r.p60, r.p61, r.p62, c.conclusion, c.fecha, c.tempo FROM preguntas p, prueba r, conclusionp c WHERE r.rut='$rut' and c.numero=p.numero;");

            while($filas=$consulta->fetch(PDO::FETCH_ASSOC))
            {
                $this->pasaje[]=$filas;
            }

            return $this->pasaje;            
        }
        
        public function get_conclusion_dominio($ruta)
        {
            $id=@$_POST['id'];
            $id=rtrim($id);
            
            $rut;

            foreach($ruta as $sub):
                $rut=$sub["rut"];
            endforeach;

            $consulta=$this->db->query("SELECT p.numero, p.sector, p.conclusion2, c.conclusion, c.tempo, c.fecha FROM conclusion_intermedia p, conclusion_mala c WHERE p.numero=c.numero;");

            while($filas=$consulta->fetch(PDO::FETCH_ASSOC))
            {
                $this->pasaje[]=$filas;
            }

            return $this->pasaje;            
        }
    
        public function get_empresas()
        {
            $consulta=$this->db->query("SELECT id, nombre, rut FROM empresa;");

            while($filas=$consulta->fetch(PDO::FETCH_ASSOC))
            {
                $this->pasaje[]=$filas;
            }

            return $this->pasaje;            
        }
    
        public function get_valida_empresa($inicio)
        {
            //$id=@$_POST['rut'];
            //$id=rtrim($id);
            
            $empresa=@$_POST['empresa'];
            $empresa=rtrim($empresa);
            
            //$rut;

            //foreach($ruta as $sub):
            //    $rut=$sub["rut"];
            //endforeach;
            
            $consulta=$this->db->query("SELECT u.name FROM usuarios u, usuario_empresa t, empresa e WHERE u.rut='$inicio' and u.rut=t.rute and t.empresa='$empresa' and t.empresa=e.nombre;");

            $t=0;
            
            while($filas=$consulta->fetch(PDO::FETCH_ASSOC))
            {
                $t++;
            }


            return $t;            
        }
    
        public function get_preguntas_empresa($pregunta)
        {
            $id=@$_POST['persona'];
            $id=rtrim($id);
            
            $empresa=@$_POST['empresa'];
            $empresa=rtrim($empresa);

            $vector=[];
            $titulo=0;
            $bandera=0;
            $i=0;
            $aux=0;
            
            $vector[0]=0;
            
            foreach($pregunta as $sub):
                
                if($bandera==0)
                {
                    $titulo=$sub["sector"];
                    
                    $aux++;
                    $bandera++;
                    
                }
                else
                {
                    if($titulo==$sub["sector"])
                    {
                        $aux++;
                        $bandera++;
                    }
                    else
                    {
                        $vector[$i]=$aux;
                        $aux=0;
                        $bandera++;
                        $i++;
                        
                        $titulo=$sub["sector"];
                        $aux++;
                    }
                }
                
            endforeach;
            
            $vector[$i]=$aux;
            
            return $vector;    
        }
    
        public function get_respuestas_empresa()
        {
            $id=@$_POST['persona'];
            $id=rtrim($id);
            
            $empresa=@$_POST['empresa'];
            $empresa=rtrim($empresa);

            
            $consulta=$this->db->query("SELECT r.p1, r.p2, r.p3, r.p4, r.p5, r.p6, r.p7, r.p8, r.p9, r.p10, r.p11, r.p12, r.p13, r.p14, r.p15, r.p16, r.p17, r.p18, r.p19, r.p20, r.p21, r.p22, r.p23, r.p24, r.p25, r.p26, r.p27, r.p28, r.p29, r.p30, r.p31, r.p32, r.p33, r.p34, r.p35, r.p36, r.p37, r.p38, r.p39, r.p40, r.p41, r.p42, r.p43, r.p44, r.p45, r.p46, r.p47, r.p48, r.p49, r.p50, r.p51, r.p52, r.p53, r.p54, r.p55, r.p56, r.p57, r.p58, r.p59, r.p60, r.p61, r.p62 FROM prueba r, usuario_empresa t  WHERE r.rut=t.rute and t.empresa='$empresa';");

            $t=0;
            
            while($filas=$consulta->fetch(PDO::FETCH_ASSOC))
            {
                $t++;
                $this->pasaje[]=$filas;
            }

            if($t==0)
            {
                return 0;
            }
            else
            {
                return $this->pasaje;
            }       
        }
    
        public function get_final($preguntas, $respuestas)
        {
            $titulo=0;
            $i=0;
            $j=1;
            $vectorm=[];
            $vectorb=[];
            $aux=0;
            $max=0;
            $buenas=0;
            $malas=0;
            
            for($j=0; $j<sizeof($preguntas); $j++)
            {
                $max=$preguntas[$j]+$max;
                $vectorm[$j]=0;
                $vectorb[$j]=0;
            }
          
            foreach($respuestas as $sub):
                
                for($j=0; $j<$max; $j++)
                {
                    if($aux==$preguntas[$i])
                    {
                        $aux=0;
                        $vectorb[$i]=$vectorb[$i]+$buenas;
                        $vectorm[$i]=$vectorm[$i]+$malas;
                        
                        $buenas=0;
                        $malas=0;
                        $i++;
                        
                        $titulo="p".strval($j+1);
                        
                        if($sub[$titulo]==2)
                        {
                            $malas++;
                        }
                        
                        if($sub[$titulo]==0)
                        {
                            $malas++;
                        }
                        
                        if($sub[$titulo]==1)
                        {
                            $buenas++;
                        }
                        
                        $aux++;
                    }
                    else
                    {
                        $titulo="p".strval($j+1);
                        
                        if($sub[$titulo]==2)
                        {
                            $malas++;
                        }
                        
                        if($sub[$titulo]==0)
                        {
                            $malas++;
                        }
                        
                        if($sub[$titulo]==1)
                        {
                            $buenas++;
                        }
                        $aux++;
                    }
                }
            
            
                $vectorb[$i]=$vectorb[$i]+$buenas;
                $vectorm[$i]=$vectorm[$i]+$malas;
                $malas=0;
                $buenas=0;
                $i=0;
                $aux=0;
            
            endforeach;
            
            $vectorm=array_merge($vectorb,$vectorm);
            
            return $vectorm;
        }
    }
       
?>