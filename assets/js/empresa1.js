function inicio()
{
    $("#colador").append("<br><div id='validar' style='text-align:center;padding:10px;'><a href='#' id='dominios_res' style='text-decoration:none;font-family:century gothic;'>Validar <i class='fa fa-user'></i></a></div>");
    
    $.ajax(
    {
        url: '../controlador/6.1 listar_empresas.php',
        type: 'POST',
        data: {}, 
        dataType: 'json',
    })
    
    .done(function(data) 
    {
        $("#empresas_s").append("<option selected value='0'>------</option>");

        data.forEach(function(item)
        {
           $("#empresas_s").append("<option value='" + item['nombre'] + "'>" + item['nombre'] + "</option>");
        }); 
    })

    .fail(function() 
    {
        alert("Error al listar!");
    });    
}

$(document).ready(function(){
    
    $('body').on('click', '#dominios_res', function()
    {
        var userx = document.getElementById('margen');
        var clase = userx.getAttribute('class');
        var select = document.getElementById('empresas_s').value;
        var userx = document.getElementById('empresas_in').value;
        bandera=0;
        
        $.ajax(
        {
            url: '../controlador/6.3 valida rut.php',
            type: 'POST',
            data: {'rut':userx}, 
            dataType: 'json',
        })
    
        .done(function(data) 
        {
            if(select=='0')
            {
                imagen = document.getElementById('borradors');
                padre = imagen.parentNode;
		        padre.removeChild(imagen);
                
                $("#cuerpo_preguntas").append("<div id='borradors'><span id='borrador_span'>¡Error! <img src='../assets/images/Error.png'></span><br><br><span id='borrador_span2'>No ha seleccionado una empresa!<br><br>Para confirmar su rut es necesario seleccionar una empresa</span><br><br><hr></div>");
            }
            else
            {
                imagen = document.getElementById('borradors');
                padre = imagen.parentNode;
		        padre.removeChild(imagen);
                
                $("#cuerpo_preguntas").append("<div id='borradors'><span id='borrador_span'>Empresa seleccionada <img src='../assets/images/Comprobado.png'></span><br><br><hr></div>");
            }
   
            if(data==0)
            {
                imagen = document.getElementById('borrador');
                padre = imagen.parentNode;
		        padre.removeChild(imagen);
                
                $("#cuerpo_preguntas").append("<div id='borrador'><span id='borrador_span'>¡Error! <img src='../assets/images/Error.png'></span><br><br><span id='borrador_span2'>El rut ingresado es invalido o no pertenece a la empresa. <br><br>Solo usuarios pertenecientes a la empresa pueden tener acceso a esta informacion.</span></div>");
                
            }
            if(data>0)
            {
                imagen = document.getElementById('borrador');
                padre = imagen.parentNode;
		        padre.removeChild(imagen);
                
                $("#cuerpo_preguntas").append("<div id='borrador'><span id='borrador_span'>¡Comprobado! <img src='../assets/images/Comprobado.png'></span><br><br><span id='borrador_span2'>El rut ingresado existe.<br><br>A continuacion se comprobara si es parte de la organizacion.</span></div>");
            }
            if(data<0)
            {
                imagen = document.getElementById('borrador');
                padre = imagen.parentNode;
		        padre.removeChild(imagen);
                
                $("#cuerpo_preguntas").append("<div id='borrador'><span id='borrador_span'>¡Error! <img src='../assets/images/Error.png'></span><br><br><span id='borrador_span2'>El campo de confirmacion de rut esta vacío! <br><br>Ingrese su rut para su verificacion.</span></div>");
            }
            
            if(data>0 && select!=0)
            {
                
                
                $.ajax(
                {
                    url: '../controlador/6.2 valida_empresa.php',
                    type: 'POST',
                    data: {'id':clase, 'empresa':select, 'persona':data}, 
                    dataType: 'json',
                })
    
                .done(function(data) 
                {
                    imagen = document.getElementById('borrador');
                    padre = imagen.parentNode;
		            padre.removeChild(imagen);
                    
                    $("#cuerpo_preguntas").append("<div id='borrador'><span id='borrador_span'>¡Rut aprobado! <img src='../assets/images/Comprobado.png'></span><br><br><hr></div>");
                    
                    
                    
                    $.ajax(
                    {
                        url: '../controlador/6.4 listar_empresa.php',
                        type: 'POST',
                        data: {'id':clase, 'empresa':select, 'persona':data}, 
                        dataType: 'json',
                    })
    
                    .done(function(data) 
                    {
                        alert(data);
                        
                        var i=0;
                        
                        /*alert("chuto");
                        
                        for(i=0; i<data.length; i++)
                        {
                            alert(data[i]);
                        }*/
                    })

                    .fail(function() 
                    {
                        alert("Error al listar!");
                    });
                    
                    
                })

                .fail(function() 
                {
                    alert("Error al listar!");
                });
                
                
                
                
                
                
            }
        })

        .fail(function() 
        {
            alert("Error al listar!");
        });
        
        /*$.ajax(
        {
            url: '../controlador/6.2 valida_empresa.php',
            type: 'POST',
            data: {'id':clase, 'empresa':select, 'persona':userx}, 
            dataType: 'json',
        })
    
        .done(function(data) 
        {
            data.forEach(function(item)
            {

            }); 
        })

        .fail(function() 
        {
            alert("Error al listar!");
        });*/
    });
    
});
