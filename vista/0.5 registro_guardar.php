<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>Encuesta de Seguridad</title>

      <link rel="stylesheet" href="../assets/css/Exito.css">
      <link rel="stylesheet" href="../assets/css/font-awesome.min.css">
      <script src="../assets/js/jquery-3.2.1.js"></script>

    </head>
    
    <body>

        <img id="fondo" src="../assets/images/fondo.png" alt="background image" />

        <div class="container">
            <div class="wrapper">

                <ul class="steps">
                    <li class="is-active">Registro completado!</li>
                </ul>

                <form action="#" method="post" class="form-wrapper">

                    <fieldset class="section is-active">
                        <h3>¡Felicidades!, su cuenta ha sido creada con éxito.</h3>

                        <div class = "mensaje7" id="mensaje7">
                            <img src="../assets/images/exito.jpg" class= "exito" id="exito" />
                            <span> Para volver al inicio de sesión presione "volver". </span>
                        </div>
               
                        <div class="atras2"><a href="../index.php">Volver</a></div>
                        <!--<div class="atras2">Volver</div>-->      
                    </fieldset>
                                
                </form>
            </div>
        </div>

    </body>

</html>